package com.example.jvazquez.camaratest;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.example.jvazquez.camaratest.Camara.AlbumBase;
import com.example.jvazquez.camaratest.Camara.AlbumPhoto;
import com.example.jvazquez.camaratest.Camara.AlbumStorage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class MainActivity extends Activity {

    private static final int ACTION_TAKE_PHOTO_B = 1;
    private ImageView imageView_fotoEvento;
    private Bitmap bitmap;
    private String mCurrentPhotoPath;
    private static final String JPEG_FILE_PREFI = "IMG_";
    private static final String JPEG_FILE_SUFI = ".jpg";
    private AlbumStorage mAlbumStorageFactory = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView_fotoEvento = (ImageView)findViewById(R.id.imageView1);
        bitmap = null;
        Permisos();
    }

    private void Permisos(){
        try{
            Button button = (Button)findViewById(R.id.btnIntend);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    TakePicture(ACTION_TAKE_PHOTO_B);

                }
            });

        }catch(Exception e){
            Toast.makeText(this, "Conceder permisos a la cámara en configuración", Toast.LENGTH_SHORT).show();
        }if (Build.VERSION.SDK_INT >= 16){
            mAlbumStorageFactory = new AlbumPhoto();
        }else{
            mAlbumStorageFactory = new AlbumBase();
        }
    }

    private String getAlbumName(){
        return getString(R.string.app_name);
    }

    private File getAlbumDir(){
        File storageDir = null;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())){
            storageDir = mAlbumStorageFactory.getAlbumStorageDir(getAlbumName());
            if (storageDir != null){
                if (!storageDir.mkdir()){
                    if (!storageDir.exists()){
                        Log.d("Camara prueba", "Falla en crear el directorio");
                    }
                }
            }
        }else{
            Log.v(getString(R.string.app_name), "El almacenamiento externo no está montado Leer/Escribir.");
        }
        return storageDir;
    }

    private File getOutputMediaFile() throws IOException{
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fileName = JPEG_FILE_PREFI + timeStamp + "_";
        File fAlbum = getAlbumDir();
        File fImage = File.createTempFile(fileName, JPEG_FILE_SUFI, fAlbum);
        return fImage;
    }

    private File setPhotoFile() throws IOException{
        File file = getOutputMediaFile();
        mCurrentPhotoPath = file.getAbsolutePath();
        return file;
    }

    private void galleryAddPic(){
        Intent mediaScan = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File file = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(file);
        mediaScan.setData(contentUri);
        this.sendBroadcast(mediaScan);
    }

    private void TakePicture(int actionCode){
        Intent intentTake = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        switch (actionCode){
            case ACTION_TAKE_PHOTO_B:
                File file = null;
                try{
                    file = setPhotoFile();
                    mCurrentPhotoPath = file.getAbsolutePath();
                    intentTake.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                } catch (IOException e) {
                    e.printStackTrace();
                    file = null;
                    mCurrentPhotoPath = null;
                }
                break;
        }
        startActivityForResult(intentTake, actionCode);
    }

    private void handleBigCameraPhoto(){
        if (mCurrentPhotoPath != null){
            galleryAddPic();
            mCurrentPhotoPath = null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case ACTION_TAKE_PHOTO_B:
                if (resultCode == RESULT_OK){
                    imageView_fotoEvento.setImageBitmap(bitmap);
                    imageView_fotoEvento.setVisibility(View.VISIBLE);
                    handleBigCameraPhoto();
                    finish();
                    Toast.makeText(this, "Imagen guardada en galeria", Toast.LENGTH_SHORT).show();
                }
        }
    }
}
