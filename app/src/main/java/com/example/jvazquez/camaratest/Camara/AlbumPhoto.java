package com.example.jvazquez.camaratest.Camara;

import android.os.Environment;

import java.io.File;

/**
 * Created by jvazquez on 12/09/17.
 */

public class AlbumPhoto extends AlbumStorage {
    @Override
    public File getAlbumStorageDir(String album) {
        return new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), album);
    }
}
