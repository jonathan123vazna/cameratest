package com.example.jvazquez.camaratest.Camara;

import android.os.Environment;

import java.io.File;

/**
 * Created by jvazquez on 12/09/17.
 */

public class AlbumBase extends AlbumStorage {

    private static final String CAMERA = "/fotito/";

    @Override
    public File getAlbumStorageDir(String album) {
        return new File(Environment.getExternalStoragePublicDirectory(album) + CAMERA + album);
    }
}
