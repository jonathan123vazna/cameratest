package com.example.jvazquez.camaratest.Camara;

import java.io.File;

/**
 * Created by jvazquez on 12/09/17.
 */

public abstract class AlbumStorage {
    public abstract File getAlbumStorageDir(String album);
}
